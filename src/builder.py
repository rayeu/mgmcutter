import time

from logger import autolog
from jenkinsutils import JenkinsBuilder, get_ordered_service_list, get_job_name_from_service_name
from svnutils import get_new_branch_name


def start_build(args, version_info, hm):
    if not args["jenkins_release_build"]:
        autolog('start_build', "", "", "Skipping jenkins build as per config.")
        return
    # Now trigger the build jobs for the specified services.
    jenkins_config = args["jenkins_config"]
    builder = JenkinsBuilder(jenkins_config["server"], jenkins_config["port"], jenkins_config["username"], jenkins_config["password"])
    try:
        builder.connect()
        builder.check_connection()
    except Exception, e:
        raise("Error connecting to Jenkins server; %s", e)

    for service in get_ordered_service_list():
        if service in version_info.keys():
            if not hm.is_jenkins_build_done(service) and version_info[service]['cut']:
                autolog('start_build', service, get_job_name_from_service_name(service), "branches/%s" %
                        get_new_branch_name(service, version_info[service]['new_version']))
                builder.run_build_job(service, "branches/%s" %
                                      get_new_branch_name(service, version_info[service]['new_version']), jenkins_config["json"])
                hm.jenkins_build_done(service)
                time.sleep(20)
            if hm.is_jenkins_build_done(service):
                autolog('start_build', service, '', "Skipping jenkins build as per config.")
