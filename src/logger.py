
import logging
import datetime


def autolog(method_name, arg1, arg2, msg):
    logging.warn("%s: %s; %s; %s; '%s'", datetime.datetime.now(), method_name, arg1, arg2, msg)
