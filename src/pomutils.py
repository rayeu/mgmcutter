"""
Utility methods to extract required information from POM files.
"""
import re

from lxml import etree

PROPERTY_PATTERN = r'aurora.(\w+).version'
SERVICE_PATTERN = r'mgm-aurora-(\w+)'

"""
This class represents a POM file for a service.
"""
class Pom:
    def __init__(self, filename):
        self.service_name = None
        self.namespaces = {'xmlns': 'http://maven.apache.org/POM/4.0.0'}
        self.filename = filename
        self.tree = etree.parse(self.filename)
        self.root = self.tree.getroot()
        self.property_map = {}
        self.dependency_map = {}

    '''
    Generate a map consisting of services on which this service is dependent on and their versions.
    '''
    def extract_property_map(self):
        properties = self.root.findall('./xmlns:properties', namespaces=self.namespaces)
        for p in properties:
            for item in p:
                found = re.search(PROPERTY_PATTERN, item.tag)
                if found:
                    dependency = 'mgm-aurora-%s' % found.group(1)
                    self.property_map[dependency] = item.text

    '''
    Returns the current version in the pom file.
    '''
    def get_current_version(self):
        return self.root.find('./xmlns:version', namespaces=self.namespaces).text

    '''
    Updates the current version to the new version.
    '''
    def update_pom_version(self, old_version, new_version, flush=False):
        element = self.root.find('./xmlns:version', namespaces=self.namespaces)
        if old_version:
            assert element.text == old_version
        element.text = new_version
        if flush:
            self.flush()

    '''
    Updated the version of depencency.
    '''
    def update_properties(self, service_name, old_version, new_version, flush=False):
        properties = self.root.findall('./xmlns:properties', namespaces=self.namespaces)
        for item in properties[0]:
            if property_from_service(service_name) in item.tag:
                if old_version:
                    assert item.text == old_version
                item.text = new_version
                break
        if flush:
            self.flush()

    def update_parent_version(self, parent_service_name, parent_new_version, flush=False):
        parent = self.root.findall('.//xmlns:parent', namespaces=self.namespaces)

        element = None
        for item in parent[0]:
            if 'artifactId' in item.tag:
                assert item.text == parent_service_name
            if 'version' in item.tag:
                element = item
        element.text = parent_new_version
        if flush:
            self.flush()

    def get_service_dependency(self):
        if not self.property_map:
            self.extract_property_map()
        return self.property_map.keys()

    def get_modules(self):
        modules = self.root.findall('.//xmlns:module', namespaces=self.namespaces)
        result = []
        for module in modules:
            result.append(module.text)
        return result

    def get_parent_version(self):
        parent = self.root.findall('./xmlns:parent', namespaces=self.namespaces)
        self.parent_info = {}
        for element in parent[0]:
            if 'version' in element.tag:
                return element.text

    '''
    Write to file.
    '''
    def flush(self):
        self.tree.write(self.filename)


"""
Given a pom file, returns the current version.
"""
def get_service_version(filename):
    pom = Pom(filename)
    return pom.get_current_version()


"""
Given a property name, returns the corresponding service.
"""
def service_from_property(property):
    found = re.search(PROPERTY_PATTERN, property)
    if found:
        return 'mgm-aurora-%s' % found.group(1)
    else:
        raise Exception("Can't derive service from property; property=%s", property)


"""
Given a service name, returns the corresponding property.
"""
def property_from_service(service):
    found = re.search(SERVICE_PATTERN, service)
    if found:
        return 'aurora.%s.version' % found.group(1)
    else:
        raise Exception("Can't derive property from service; service=%s", service)


if __name__ == "__main__":
    p = Pom("/mgm/mgm-aurora/mgm-aurora-mpbs/trunk/pom-for-test.xml")
    p.extract_property_map()
    p.update_pom_version(None, "3.8.5-SNAPSHOT", True)
    print p.get_parent_version()
    print p.property_map
    p.update_properties("mgm-aurora-framework", None, "9.9.9", True)
    print p.get_current_version()


    p = Pom("/mgm/mgm-aurora/mgm-aurora-tps-root-automation-test/trunk/pom.xml")
    print p.get_modules()
    print p.get_service_dependency()
    p.extract_property_map()
    print p.property_map
