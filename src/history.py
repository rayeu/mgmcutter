import os
import json

from logger import autolog

class ServiceHistory(object):
    def __init__(self):
        self.svn_branch_remote_copy = False
        self.svn_branch_commit = False
        self.svn_trunk_commit = False
        self.jenkins_build = False

    def get_writable_content(self):
        return {'svn_branch_remote_copy': self.svn_branch_remote_copy,
                'svn_branch_commit': self.svn_branch_commit,
                'svn_trunk_commit': self.svn_trunk_commit,
                'jenkins_build': self.jenkins_build
                }

    def parse_from_dict(self, adict):
        self.svn_branch_remote_copy = adict['svn_branch_remote_copy']
        self.svn_branch_commit = adict['svn_branch_commit']
        self.svn_trunk_commit = adict['svn_trunk_commit']
        self.jenkins_build = adict['jenkins_build']


class HistoryManager:
    def __init__(self, filename):
        self.filename = os.path.abspath(filename)
        # A map of service histories by service name
        self.history = {}

        try:
            with open(filename, "r+") as rf:
                data = json.loads(rf.read())
                for k, v in data.iteritems():
                    h = ServiceHistory()
                    h.parse_from_dict(v)
                    self.history[k] = h
            autolog("HistoryManager", "__init__", self.filename, "Successfully loaded history from file.")
        except Exception, e:
            autolog("HistoryManager", "init", "", "No history found.")

    def serialize(self):
        with open(self.filename, "w+") as wf:
            data = {}
            for n, h in self.history.iteritems():
                data[n] = h.get_writable_content()
            json.dump(data, wf)
        autolog("HistoryManager", "serialize", self.filename, "Successfully saved history to file.")

    def add_service_history(self, service_name):
        self.history[service_name] = ServiceHistory()

    def has_service_history(self, service_name):
        return service_name in self.history.keys()

    def is_svn_branch_remote_copy_done(self, service_name):
        return service_name in self.history.keys() and self.history[service_name].svn_branch_remote_copy

    def is_svn_branch_commit_done(self, service_name):
        return service_name in self.history.keys() and self.history[service_name].svn_branch_commit

    def is_svn_trunk_commit_done(self, service_name):
        return service_name in self.history.keys() and self.history[service_name].svn_trunk_commit

    def is_jenkins_build_done(self, service_name):
        return service_name in self.history.keys() and self.history[service_name].jenkins_build

    def svn_branch_remote_copy_done(self, service_name, serialize=True):
        if not self.has_service_history(service_name):
            self.add_service_history(service_name)
        self.history[service_name].svn_branch_remote_copy = True
        if serialize:
            self.serialize()

    def svn_branch_commit_done(self, service_name, serialize=True):
        if not self.has_service_history(service_name):
            self.add_service_history(service_name)
        self.history[service_name].svn_branch_commit = True
        if serialize:
            self.serialize()

    def svn_trunk_commit_done(self, service_name, serialize=True):
        if not self.has_service_history(service_name):
            self.add_service_history(service_name)
        self.history[service_name].svn_trunk_commit = True
        if serialize:
            self.serialize()

    def jenkins_build_done(self, service_name, serialize=True):
        if not self.has_service_history(service_name):
            self.add_service_history(service_name)
        self.history[service_name].jenkins_build = True
        if serialize:
            self.serialize()


if __name__ == "__main__":
    hm = HistoryManager('../config/history_file.json')
    # hm.svn_branch_remote_copy_done('mgm-aurora-pdk')
    # hm.svn_branch_commit_done('mgm-aurora-pdk')
    # hm.svn_trunk_commit_done('mgm-aurora-pdk')
    # hm.jenkins_build_done('mgm-aurora-pdk')
    #
    # hm.svn_branch_commit_done('mgm-aurora-rds')
    # hm.jenkins_build_done('mgm-aurora-rds')

    print hm.is_svn_branch_remote_copy_done('mgm-aurora-pdk')
    print hm.is_svn_branch_commit_done('mgm-aurora-pdk')
    print hm.is_svn_trunk_commit_done('mgm-aurora-pdk')
    print hm.is_jenkins_build_done('mgm-aurora-pdk')

    print hm.is_jenkins_build_done('mgm-aurora-rds')
