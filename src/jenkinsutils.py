
import os
import jenkins

from logger import autolog

ordered_jenkins_jobs_list = [
    ("mgm-aurora-parent", "00_PS_PARENT_JDK7_RELEASE"),
    ("mgm-aurora-framework", "01_PS_FRAMEWORK_JDK7_RELEASE"),
    ("mgm-aurora-rds", "02_PS_RDS_JDK7_RELEASE"),
    ("mgm-aurora-cs", "03_PS_CS_JDK7_RELEASE"),
    ("mgm-aurora-tps-root", "04_PS_TPS-ORIS_JDK7_RELEASE"),
    ("mgm-aurora-aris", "05_PS_ARIS_JDK7_RELEASE"),
    ("mgm-aurora-paris", "06_PS_PARIS_JDK7_RELEASE"),
    ("mgm-aurora-orbs", "07_PS_ORBS_JDK7_RELEASE"),
    ("mgm-aurora-opbs", "08_PS_OPBS_JDK7_RELEASE"),
    ("mgm-aurora-oebs", "09_PS_OEBS_JDK7_RELEASE"),
    ("mgm-aurora-pbs", "10_PS_PBS_JDK7_RELEASE"),
    ("mgm-aurora-obbs", "11_PS_OBBS_JDK7_RELEASE"),
    ("mgm-aurora-eas", "12_PS_EAS_JDK7_RELEASE"),
    ("mgm-aurora-ebs", "13_PS_EBS_JDK7_RELEASE"),
    ("mgm-aurora-alerts", "14_PS_ALERTS_JDK7_RELEASE"),
    ("mgm-aurora-rmms", "15_PS_RMMS_JDK7_RELEASE"),
    ("mgm-aurora-tpws", "16_PS_TPWS_JDK7_RELEASE"),
    ("mgm-aurora-rdws", "17_PS_RDWS_JDK7_RELEASE"),
    ("mgm-aurora-rdcws", "18_PS_RDCWS_JDK7_RELEASE"),
    ("mgm-aurora-env", "19_PS_AURORA_ENV_RELEASE"),
    ("mgm-aurora-client", "19_PS_CLIENT_JDK7_RELEASE"),
    ("mgm-aurora-pdk", "20_PS_PDK_JDK7_RELEASE"),
    ("mgm-aurora-mpbs", "21_PS_MPBS_JDK_RELEASE"),
    ("mgm-aurora-orms", "22_PS_ORMS_JDK_RELEASE")
]

def get_ordered_job_list():
    for each in ordered_jenkins_jobs_list:
        yield each[1]

def get_ordered_service_list():
    for each in ordered_jenkins_jobs_list:
        yield each[0]

def get_job_name_from_service_name(service_name):
    for each in ordered_jenkins_jobs_list:
        if each[0] == service_name:
            return each[1]


class JenkinsBuilder:
    def __init__(self, server, port, username, password):
        self.server = server
        self.port = port
        self.username = username
        self.password = password
        self.builder = None

    def connect(self):
        os.environ['NO_PROXY'] = self.server
        url = "http://%s:%s" % (self.server, self.port)
        self.builder = jenkins.Jenkins(url, username=self.username, password=self.password)

    def check_connection(self):
        print self.builder.get_whoami()

    """
    Runs jenkins build job for the given service
    """
    def run_build_job(self, service_name, svn_version, deploy_json="TEST"):
        job_name = get_job_name_from_service_name(service_name)
        params = {'SVN_VERSION': svn_version, 'DEPLOY_JSON': deploy_json}
        self.builder.build_job(job_name, params)
        last_build_number = self.builder.get_job_info(job_name)['lastCompletedBuild']['number']
        build_info = self.builder.get_build_info(job_name, last_build_number)
        autolog("run_build_job", service_name, svn_version, build_info)


if __name__ == "__main__":
    j = JenkinsBuilder('vmbuild01t', 8080, 'automation', 'Automation')
    j.connect()
    j.check_connection()