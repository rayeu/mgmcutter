import os
import json
import shutil
import uuid

from pomutils import Pom
from svnutils import *
from logger import autolog

def get_snapshot_version_from_release_version(release_version):
    try:
        major_version, minor_version, increment_version = release_version.split('.')
        return '%s.%s.%s-SNAPSHOT' % (major_version, minor_version, int(increment_version) + 1)
    except ValueError:
        major_version, minor_version = release_version.split('.')
        return '%s.%s-SNAPSHOT' % (major_version, int(minor_version) + 1)
    except Exception:
        raise Exception("cutter; can't derive snapshot version from release version")


'''
Reads the json file containing all the services that need to be
cut, as well as the old and new versions.
'''
def parse_release_versions(args):
    release_versions = {}
    if os.path.exists(args['release_version_file']):
        release_versions = json.loads(open(args['release_version_file']).read())
    return release_versions


def parse_snapshot_versions(args):
    snapshot_versions = {}
    if os.path.exists(args['snapshot_version_file']):
        snapshot_versions = json.loads(open(args['snapshot_version_file']).read())
    return snapshot_versions


def parse_input(args):
    release_versions = parse_release_versions(args)
    snapshot_versions = parse_snapshot_versions(args)
    if snapshot_versions:
        release_versions.update(snapshot_versions)
        return release_versions
    # If no snapshot version is specified, then increment the snapshot
    # versions by looking at the release versions
    for service in release_versions:
        release_versions[service]['new_snapshot_version'] = \
            get_snapshot_version_from_release_version(release_versions[service]['new_version'])
    return release_versions

'''
If update_parent is True, it just updates the parent info of the POM file.
If update_parent is False, it updates all the properties mentioned in the input file.
'''
def update_pom(filename, service_name, version_info, trunk=False, update_parent=False):
    pom = Pom(filename)

    if trunk:
        new_version = version_info[service_name]['new_snapshot_version']
    else:
        new_version = version_info[service_name]['new_version']
    if update_parent:
        pom.update_parent_version(service_name, new_version)
        pom.flush()
        return pom.get_modules()

    pom.update_pom_version(None, new_version)
    autolog('update_pom', service_name, trunk, "changed version to %s" % new_version)
    pom.extract_property_map()
    # for each service dependency and its version
    for ds, v in pom.property_map.items():
        if ds in version_info.keys():
            pom.update_properties(ds, None, version_info[ds]['new_version'])
            autolog('update_pom', service_name, trunk, "service dependency %s version changed to %s" %
                    (ds, version_info[ds]['new_version']))
        elif "SNAPSHOT" in v:
            raise Exception("cutter; service=%s has a SNAPSHOT dependency=(%s, version=%s) which is not cut" % (service_name, ds, v))
        else:
            autolog('update_pom', service_name, trunk, "service dependency %s unchanged" % ds)

    mgm_aurora_parent = 'mgm-aurora-parent'
    # Check if parent is also being cut. If so, update parent dependency separately. Only do these for non-modules.
    if not update_parent and mgm_aurora_parent in version_info.keys() and service_name != mgm_aurora_parent:
        # TODO: Remove the condition once parent component is introduced in the pom file.
        # Special case for mgm-aurora-env, mgm-aurora-orms, mgm-aurora-client as it has no parent component.
        if service_name == "mgm-aurora-env" or service_name == "mgm-aurora-orms" or service_name == "mgm-aurora-client":
            pom.flush()
            return pom.get_modules()
        pom.update_parent_version(mgm_aurora_parent, version_info[mgm_aurora_parent]['new_version'])
        autolog('update_pom', service_name, trunk, 'parent dependency version changed to %s' % version_info[mgm_aurora_parent]['new_version'])
    pom.flush()
    return pom.get_modules()

'''
For the given service, updates trunk and its dependencies to the new snapshot version.
'''
def update_trunk(service_name, version_info, args, hm):
    try:
        new_snapshot_version = version_info[service_name]['new_snapshot_version']
    except Exception, e:
        raise Exception("Can't parse the input data.", e)

    try:
        original_dir = os.getcwd()
        wdir = os.path.join(os.getcwd(), 'temp', service_name, uuid.uuid1().hex)
        tdir = os.path.join(wdir, 't')
        os.makedirs(tdir)
        os.chdir(tdir)
    except:
        raise Exception("Can't create new temp directory.", e)

    try:
        trunk_url = get_svn_trunk_url_for_service(service_name)

        # checkout trunk --depth = empty
        svn_checkout_empty(trunk_url)

        os.chdir('trunk')
        # update just the pom.xml files
        svn_update_pomfiles(trunk_url)

        # update dependencies
        trunk_dir = os.path.join(tdir, 'trunk')
        trunk_pom = os.path.join(trunk_dir, 'pom.xml')

        autolog('update_trunk', service_name, "trunk", "Updating dependencies for Trunk")
        modules = update_pom(trunk_pom, service_name, version_info, trunk=True, update_parent=False)
        if modules:
            for m in modules:
                filename = os.path.join(trunk_dir, m, "pom.xml")
                update_pom(filename, service_name, version_info, trunk=True, update_parent=True)
                autolog('update_trunk', service_name, "trunk", "Updated module %s to %s" % (m, new_snapshot_version))

        if args['commit']:
            if not hm.is_svn_trunk_commit_done(service_name):
                svn_commit(trunk_url, args['commit_message'])
                hm.svn_trunk_commit_done(service_name)
            else:
                autolog("update_trunk", service_name, "trunk", "Skipped trunk commit as per history.")
        else:
            autolog("update_trunk", service_name, "trunk", "Skipped trunk commit as per config.")
    finally:
        os.chdir(original_dir)

'''
For the given service, cut a new branch with the new version
'''
def cut_new_branch(service_name, version_info, args, hm):
    try:
        new_version = version_info[service_name]['new_version']
    except Exception, e:
        raise Exception("Can't parse the input data.", e)

    try:
        original_dir = os.getcwd()
        wdir = os.path.join(os.getcwd(), 'temp', service_name, uuid.uuid1().hex)
        bdir = os.path.join(wdir, 'b')
        os.makedirs(bdir)
        os.chdir(bdir)
    except Exception, e:
        raise Exception("Can't create new temp directory.", e)

    try:
        trunk_url = get_svn_trunk_url_for_service(service_name)
        branch_url = get_svn_branch_url_for_service(service_name, get_new_branch_name(service_name, new_version))

        # copy trunk to new branch remotely
        if not hm.is_svn_branch_remote_copy_done(service_name):
            svn_remote_copy(trunk_url, branch_url, args['commit_message'])
            hm.svn_branch_remote_copy_done(service_name)
        else:
            autolog("cut_new_branch", service_name, new_version, "Skipped remote branch copy as per history.")

        # checkout the new branch --depth = empty
        svn_checkout_empty(branch_url)

        os.chdir(get_new_branch_name(service_name, new_version))
        # update just the pom.xml files
        svn_update_pomfiles(branch_url)

        # update dependencies
        branch_dir = os.path.join(bdir, get_new_branch_name(service_name, new_version))
        branch_pom = os.path.join(branch_dir, "pom.xml")

        autolog('cut_new_branch', service_name, get_new_branch_name(service_name, new_version), "Updating dependencies for Branch")
        modules = update_pom(branch_pom, service_name, version_info)

        if modules:
            for m in modules:
                filename = os.path.join(branch_dir, m, "pom.xml")
                update_pom(filename, service_name, version_info, trunk=False, update_parent=True)
                autolog('cut_new_branch', service_name, get_new_branch_name(service_name, new_version),
                    "Updated module %s to %s" % (m, new_version))

        if args['commit']:
            if not hm.is_svn_branch_commit_done(service_name):
                svn_commit(branch_url, args['commit_message'])
                hm.svn_branch_commit_done(service_name)
            else:
                autolog("cut_new_branch", service_name, new_version, "Skipped branch commit as per history.")
        else:
            autolog("cut_new_branch", service_name, new_version, "Skipped branch commit as per config.")
    finally:
        os.chdir(original_dir)


def start_cut(args, hm):
    version_info = parse_input(args)
    if args['update_branch']:
        for service, info in version_info.iteritems():
            if info["cut"]:
                cut_new_branch(service, version_info, args, hm)
    else:
        autolog('start_cut', "branch", "-", "Skipping branch cut as per config.")

    if args['update_trunk']:
        for service, info in version_info.iteritems():
            if info["cut"]:
                update_trunk(service, version_info, args, hm)
    else:
        autolog('start_cut', "trunk", "-", "Skipping trunk update as per config.")

    if args['cleanup']:
        temp_path = os.path.join(os.getcwd(), 'temp')
        if temp_path:
            shutil.rmtree(temp_path)
            autolog('start_cut', "cleanup", temp_path, "Successfully cleaned up temp dir.")
    return version_info
