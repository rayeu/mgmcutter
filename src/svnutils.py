import subprocess

from logger import autolog

trunk_url_template = "https://buildit.mgmresorts.com/svn/MGMRI/Platform/Aurora/mgm-aurora/%s/trunk"
branch_url_template = "https://buildit.mgmresorts.com/svn/MGMRI/Platform/Aurora/mgm-aurora/%s/branches/%s"


def get_svn_trunk_url_for_service(service_name):
    return trunk_url_template % service_name


def get_svn_branch_url_for_service(service_name, branch_name):
    return branch_url_template % (service_name, branch_name)


def get_new_branch_name(service_name, new_version):
    service_core_name = service_name.split('-')[2]
    return "%s_%s" % (service_core_name, new_version)


def svn_remote_copy(from_url, to_url, msg):
    p = subprocess.Popen(['svn', 'copy', from_url, to_url, '-m', msg])
    p.communicate()
    if p.returncode:
        raise Exception("svn copy %s %s failed", from_url, to_url)
    else:
        autolog('svn_remote_copy', from_url, to_url, "Successfully created new branch.")


def svn_checkout_empty(url):
    p = subprocess.Popen(['svn', 'co', url, '--depth', 'empty'])
    p.communicate()
    if p.returncode:
        raise Exception("svn co %s --depth empty failed", url)
    else:
        autolog('svn_checkout_empty', url, "--depth empty", "Successfully checked out new branch.")


def svn_update_pomfiles(url):
    p = subprocess.Popen('svn up --parents `svn list --recursive %s | grep "pom.xml"`' % url,
                         stdout=subprocess.PIPE, shell=True)
    p.communicate()
    if p.returncode:
        raise Exception("svn up --parents `svn list --recursive %s | grep 'pom.xml'` failed. "
                        "Note: windows doesn't support grep!" % url)
    else:
        autolog('svn_update_pomfiles', url, "grep pomfiles", "Successfully updated pom.xmls from SVN.")


def svn_commit(url, msg):
    p = subprocess.Popen(['svn', 'commit', "-m", msg])
    p.communicate()
    if p.returncode:
        raise Exception("svn commit failed")
    else:
        autolog('svn_commit', url, msg, "Successfully committed changes to SVN.")
