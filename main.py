import argparse
import json

from src.builder import start_build
from src.cutter import start_cut
from src.history import HistoryManager


def parse_config():
    parser = argparse.ArgumentParser()
    parser.add_argument("config", help="config.json used by the cutter.")
    args = parser.parse_args()
    return json.loads(open(args.config).read())


def main():
    args = parse_config()
    hm = HistoryManager(args['history_file'])
    info = start_cut(args, hm)
    start_build(args, info, hm)
    hm.serialize()


main()
